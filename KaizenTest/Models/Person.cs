﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KaizenTest.Models
{
    public class Person
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public int PersonalIdentificationNumber { get; set; }

        [Required]
        [MaxLength(25)]
        public string FirtsName { get; set; }

        [MaxLength(25)]
        public string MiddleName { get; set; }

        [Required]
        [MaxLength(25)]
        public string FirstSurname { get; set; }

        [MaxLength(25)]
        public string SecondSurname { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }

        [Required]
        public int GenderFK { get; set; }

        [Required]
        [ForeignKey("GenderFK")]
        public virtual Gender Gender { get; set; }

        public int CountryFK { get; set; }

        [Required]
        [ForeignKey("CountryFK")]
        public virtual Country Country { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}
