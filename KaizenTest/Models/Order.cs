﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KaizenTest.Models
{
    public class Order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public int PersonFK { get; set; }

        [Required]
        public int ProductFK { get; set; }

        [Required]
        public decimal Quantity { get; set; }

        [Required]
        public DateTime OrderDate { get; set; }

        [Required]
        [ForeignKey("PersonFK")]
        public virtual Person Person { get; set; }

        [Required]
        [ForeignKey("ProductFK")]
        public virtual Product Product { get; set; }
    }
}