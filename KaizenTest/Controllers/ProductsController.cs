﻿using KaizenTest.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace KaizenTest.Controllers
{
    public class ProductsController : BaseController
    {
        public ProductsController(AppDbContext context) : base(context) { }

        public async Task<IActionResult> Index(int? pageNumber)
        {
            IPagedList<Product> data = await context.Products.ToPagedListAsync(pageNumber ?? 1, ITEMS_BY_PAGE);
            return View(data);
        }
    }
}
