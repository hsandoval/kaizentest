﻿using KaizenTest.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace KaizenTest.Controllers
{
    public class PeopleController : BaseController
    {
        public PeopleController(AppDbContext context) : base(context) { }

        public async Task<IActionResult> Index(int? pageNumber)
        {
            IPagedList<Person> data = await context.People.ToPagedListAsync(pageNumber ?? 1, ITEMS_BY_PAGE);
            return View(data);
        }
    }
}
