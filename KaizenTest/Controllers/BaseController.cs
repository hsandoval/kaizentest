﻿using KaizenTest.Models;
using Microsoft.AspNetCore.Mvc;

namespace KaizenTest.Controllers
{
    public abstract class BaseController : Controller
    {
        protected readonly AppDbContext context;
        protected const int ITEMS_BY_PAGE = 10;

        public BaseController(AppDbContext context)
        {
            this.context = context;
        }
    }
}
