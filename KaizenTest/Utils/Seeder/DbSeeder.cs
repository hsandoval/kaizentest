﻿using Bogus;
using KaizenTest.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace KaizenTest.Utils.Seeder
{
    public class DbSeeder
    {
        private readonly AppDbContext dbContext;
        private readonly IHostingEnvironment hosting;
        private readonly IConfiguration configuration;

        private readonly DateTime CurrentDateTime = DateTime.Now;
        private IDictionary<string, object> DictionaryJsonFile;

        public DbSeeder(AppDbContext dbContext, IHostingEnvironment hosting, IConfiguration configuration)
        {
            this.dbContext = dbContext;
            this.configuration = configuration;
            this.hosting = hosting;
        }

        public async Task SeedAsync()
        {
            await dbContext.Database.EnsureDeletedAsync();
            await dbContext.Database.EnsureCreatedAsync();
            await FillDatabaseAsync();
        }

        private async Task FillDatabaseAsync()
        {
            string pathJsonFileSeederDatabase = configuration.GetValue<string>("fileData");
            string filePath = Path.Combine(hosting.ContentRootPath, pathJsonFileSeederDatabase);
            string json = await File.ReadAllTextAsync(filePath);
            DictionaryJsonFile = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
            await PopulateData<Country>("Countries");
            await PopulateData<Gender>("Genders");
            await PopulateDataProducts();
            await PopulateDataPersons();
        }

        private async Task PopulateData<TEntity>(string nameKey) where TEntity : class
        {
            try
            {
                DbSet<TEntity> dbSet = dbContext.Set<TEntity>();
                if (!dbSet.Any())
                {
                    if (DictionaryJsonFile.ContainsKey(nameKey))
                    {
                        IEnumerable<TEntity> listItems = JsonConvert.DeserializeObject<IEnumerable<TEntity>>(DictionaryJsonFile[nameKey].ToString());
                        await dbSet.AddRangeAsync(listItems);
                        await dbContext.SaveChangesAsync();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private async Task PopulateDataProducts()
        {
            try
            {
                if (!dbContext.Products.Any())
                {
                    Faker<Product> fakerProducts = new Faker<Product>()
                        .RuleFor(p => p.Description, f => f.Commerce.Product())
                        .RuleFor(p => p.UnitCost, f => f.Random.Decimal(1, 100));

                    dbContext.Products.AddRange(fakerProducts.GenerateLazy(50).GroupBy(x => x.Description).Select(x => x.First()));
                    await dbContext.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private async Task PopulateDataPersons()
        {
            try
            {
                if (!dbContext.People.Any())
                {
                    dbContext.People.AddRange(GetSamplePeopleData());
                    await dbContext.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private IEnumerable<Models.Person> GetSamplePeopleData()
        {
            int quantity = 1000;
            Random random = new Random();

            IEnumerable<int> idGenders = dbContext.Genders.ToList().Select(i => i.Id);
            IEnumerable<int> idCountries = dbContext.Country.ToList().Select(i => i.Id);
            IEnumerable<int> idProducts = dbContext.Products.ToList().Select(i => i.Id);

            Faker<Order> fakerOrders = new Faker<Order>()
                .RuleFor(o => o.OrderDate, f => f.Date.Recent(10))
                .RuleFor(o => o.ProductFK, f => f.PickRandom(idProducts))
                .RuleFor(o => o.Quantity, f => f.Random.Int(1, 10));

            Faker<Models.Person> fakerPerson = new Faker<Models.Person>()
                .RuleFor(p => p.GenderFK, f => f.PickRandom(idGenders))
                .RuleFor(p => p.PersonalIdentificationNumber, f => f.Random.Number(12000000, 30000000))
                .RuleFor(p => p.FirtsName, (f, p) => f.Person.FirstName)
                .RuleFor(p => p.MiddleName, (f, p) => f.Person.FirstName)
                .RuleFor(p => p.FirstSurname, (f, p) => f.Person.LastName)
                .RuleFor(p => p.SecondSurname, (f, p) => f.Person.LastName)
                .RuleFor(p => p.BirthDate, f => f.Person.DateOfBirth)
                .RuleFor(p => p.CountryFK, f => f.PickRandom(idCountries))
                .RuleFor(p => p.Orders, fakerOrders.Generate(random.Next(25)));

            List<Models.Person> people = fakerPerson.Generate(quantity).GroupBy(x => x.PersonalIdentificationNumber).Select(x => x.First()).ToList();

            return people;
        }
    }
}
